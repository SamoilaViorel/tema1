﻿using CustomerService.Protos;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerService.Services
{
    public class CustomerServer : CustomerConnection.CustomerConnectionBase
    {
        private readonly ILogger<CustomerServer> _logger;
        public CustomerServer(ILogger<CustomerServer> logger)
        {
            _logger = logger;
        }

        public int TakeAgeFromCNP(Customer customer)
        {
            int year = TakeYearFromCNP(customer.Cnp);
            int month = TakeMonthFromCNP(customer.Cnp);
            int day = TakeDayFromCNP(customer.Cnp);

            int age = DateTime.UtcNow.Year - year;
            if(DateTime.UtcNow.Month<month)
            {
                return --age;
            }
            else if(DateTime.UtcNow.Month == month && DateTime.UtcNow.Day<day)
            {
                return --age;
            }
            return age;
        }

        private int TakeDayFromCNP(string cnp)
        {
            if (cnp[5] == '0')
                return cnp[6] - '0';
            return (cnp[5] - '0') * 10 + (cnp[6] - '0');
        }

        private int TakeMonthFromCNP(string cnp)
        {
            if (cnp[3] == '0')
                return cnp[4] - '0';
            return (cnp[3] - '0') * 10 + (cnp[4] - '0');
        }

        private int TakeYearFromCNP(string cnp)
        {
            if (cnp[0] == '1' || cnp[0] == '2')
            {
                return 1900 + ((cnp[1] - '0') * 10 + (cnp[2] - '0'));
            }
            else
            {
                return 2000 + ((cnp[1] - '0') * 10 + (cnp[2] - '0'));
            }


        }


        private string TakeGenderFromCNP(string cnp)
        {
            if (cnp[0] == '1' || cnp[0] == '5')
            {
                return "Male";
            }
            else
            {
                return "Female";
            }


        }

        public override Task<AuthentificationResponse> Authentification(AuthentificationRequest request, ServerCallContext context)
        {
            Console.WriteLine("Name " + request.Customer.Name);
            Console.WriteLine("CNP " + request.Customer.Cnp);
            Customer customer = new Customer()
            {
                Name = request.Customer.Name,
                Cnp = request.Customer.Cnp
            };
            Console.WriteLine("Age "+ TakeAgeFromCNP(customer));
            Console.WriteLine("Gender "+TakeGenderFromCNP(customer.Cnp));

            return Task.FromResult(new AuthentificationResponse() { Status = AuthentificationResponse.Types.Status.Succes });
        }

       
    }
}
