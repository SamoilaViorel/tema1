﻿using CustomerService.Protos;
using Grpc.Net.Client;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace CustomerClient
{
    class Program
    {
        public static bool ValidationName(string name)
        {
            if(name=="")
               return false;
            return true;
        }

        public static bool ValidationCnp(string cnp)
        {
            bool forDigit = cnp.All(char.IsDigit);
            if (!forDigit)
                return false;
            if (cnp.Length != 13)
                return false;
            if (cnp[0] != '1' && cnp[0] != '2' && cnp[0]!='5' && cnp[0]!='6')
                return false;
            return true;
        }


        static async Task Main(string[] args)
        {
            var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new CustomerConnection.CustomerConnectionClient(channel);

            Console.WriteLine("Name ");
            var name = Console.ReadLine();
            
            while(!ValidationName(name))
            {
                Console.WriteLine("Enter a valid name ");
                name = Console.ReadLine();
            }

            Console.WriteLine("CNP ");
            var cnp = Console.ReadLine();

            while (!ValidationCnp(cnp))
            {
                Console.WriteLine("Enter a valid  ");
                cnp = Console.ReadLine();
            }

            var CustomerAdd = new Customer()
            {
                Name = name,
                Cnp = cnp
            };

            var response = await client.AuthentificationAsync(new AuthentificationRequest { Customer = CustomerAdd });

            Console.WriteLine(response.Status.ToString());
        }
    }
}
